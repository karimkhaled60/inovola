import 'package:flutter/material.dart';
import 'package:innovola/provider/provider.dart';
import 'package:innovola/widgets/course_details.dart';
import 'package:innovola/widgets/order_button.dart';
import 'package:innovola/widgets/slider.dart';
import 'package:provider/provider.dart';

class ItemDetailsPage extends StatelessWidget {
  const ItemDetailsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ChangeNotifierProvider<ItemDetailsProvider>(
          create: (_) => ItemDetailsProvider(),
          builder: (context, snapshot) {
            return Consumer<ItemDetailsProvider>(builder: (context, provider, _) {
              if (provider.place == null) {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              }

              return ListView(padding: const EdgeInsets.all(0), children: <Widget>[
                CustomSlider(provider),
                Padding(padding: const EdgeInsets.symmetric(horizontal: 24), child: Text(provider.place?.interest ?? "")),
                Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 24),
                    child: Text(provider.place?.title ?? "", style: const TextStyle(fontSize: 20, color: Color(0xff9ea3b8)))),
                const SizedBox(height: 10),
                CourseDetails(provider.place?.date ?? "", Icons.calendar_month),
                CourseDetails(provider.place?.address ?? "", Icons.pin_drop),
                _divider(),
                ..._trainerDataWidgets(provider),
                _divider(),
                const Padding(padding: EdgeInsets.symmetric(horizontal: 24), child: Text("عن الدورة")),
                Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 24),
                    child: Text(provider.place?.occasionDetail ?? "", textAlign: TextAlign.right)),
                _divider(),
                const Padding(padding: EdgeInsets.symmetric(horizontal: 24), child: Text("تكلفة الدورة")),
                for (int i = 0; i < provider.place!.reservTypes!.length; i++)
                  _priceRowItem(provider.place!.reservTypes![i].price ?? 0, provider.place!.reservTypes![i].name ?? ""),
                const SizedBox(height: 24),
                const OrderButton()
              ]);
            });
          }),
    );
  }

  _trainerDataWidgets(provider) => [
        Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24),
            child: Row(children: <Widget>[
              ClipOval(
                clipBehavior: Clip.antiAlias,
                child: Container(
                    color: Colors.white,
                    width: 60,
                    height: 60,
                    child: Image.network(
                      provider.place?.trainerImg ?? "",
                      errorBuilder: (_, __, ___) => Image.network(
                        "https://thumbs.dreamstime.com/z/four-friends-hands-up-creers-vecation-180552444.jpg",
                        fit: BoxFit.fill,
                      ),
                    )),
              ),
              const SizedBox(width: 30),
              Text(provider.place?.trainerName ?? "")
            ])),
        Padding(padding: const EdgeInsets.symmetric(horizontal: 24), child: Text(provider.place?.trainerInfo ?? "", textAlign: TextAlign.right)),
      ];

  _priceRowItem(int price, String text) => Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[Expanded(child: Text(text, textAlign: TextAlign.right)), Text("SAR $price")]),
      );

  _divider() => Container(height: 0.5, width: double.infinity, margin: const EdgeInsets.symmetric(vertical: 10), color: Colors.grey);
}
