import 'package:flutter/material.dart';
import 'package:innovola/model/place.dart';
import 'package:innovola/data_source/repo.dart';

class ItemDetailsProvider extends ChangeNotifier {
  bool isLoading = false;
  Place? place;
  int currentImage = 0;
  final PageController controller = PageController();

  ItemDetailsProvider() {
    _getData();
  }
  onPageChange(int page) {
    currentImage = page;
    notifyListeners();
  }

  _getData() async {
    isLoading = true;
    notifyListeners();
    place = await getData().then(Place.fromJson);
    isLoading = false;
    notifyListeners();
  }
}
