class Place {
  int? id;
  String? title;
  List<String>? img;
  String? interest;
  int? price;
  String? date;
  String? address;
  String? trainerName;
  String? trainerImg;
  String? trainerInfo;
  String? occasionDetail;
  String? latitude;
  String? longitude;
  bool? isLiked;
  bool? isSold;
  bool? isPrivateEvent;
  bool? hiddenCashPayment;
  int? specialForm;
  List<ReservTypes>? reservTypes;

  Place(
      {this.id,
      this.title,
      this.img,
      this.interest,
      this.price,
      this.date,
      this.address,
      this.trainerName,
      this.trainerImg,
      this.trainerInfo,
      this.occasionDetail,
      this.latitude,
      this.longitude,
      this.isLiked,
      this.isSold,
      this.isPrivateEvent,
      this.hiddenCashPayment,
      this.specialForm,
      this.reservTypes});

  Place.fromJson(json) {
    id = json['id'];
    title = json['title'];
    img = json['img'].cast<String>();
    interest = json['interest'];
    price = json['price'];
    date = json['date'];
    address = json['address'];
    trainerName = json['trainerName'];
    trainerImg = json['trainerImg'];
    trainerInfo = json['trainerInfo'];
    occasionDetail = json['occasionDetail'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    isLiked = json['isLiked'];
    isSold = json['isSold'];
    isPrivateEvent = json['isPrivateEvent'];
    hiddenCashPayment = json['hiddenCashPayment'];
    specialForm = json['specialForm'];
    if (json['reservTypes'] != null) {
      reservTypes = <ReservTypes>[];
      json['reservTypes'].forEach((v) {
        reservTypes!.add(ReservTypes.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['title'] = title;
    data['img'] = img;
    data['interest'] = interest;
    data['price'] = price;
    data['date'] = date;
    data['address'] = address;
    data['trainerName'] = trainerName;
    data['trainerImg'] = trainerImg;
    data['trainerInfo'] = trainerInfo;
    data['occasionDetail'] = occasionDetail;
    data['latitude'] = latitude;
    data['longitude'] = longitude;
    data['isLiked'] = isLiked;
    data['isSold'] = isSold;
    data['isPrivateEvent'] = isPrivateEvent;
    data['hiddenCashPayment'] = hiddenCashPayment;
    data['specialForm'] = specialForm;
    if (reservTypes != null) data['reservTypes'] = reservTypes!.map((v) => v.toJson()).toList();
    return data;
  }
}

class ReservTypes {
  int? id;
  String? name;
  int? count;
  int? price;

  ReservTypes({this.id, this.name, this.count, this.price});

  ReservTypes.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    count = json['count'];
    price = json['price'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['count'] = count;
    data['price'] = price;
    return data;
  }
}
