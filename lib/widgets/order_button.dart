import 'package:flutter/material.dart';

class OrderButton extends StatelessWidget {
  const OrderButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
        child: Container(
            width: double.infinity,
            decoration: const BoxDecoration(
                gradient: LinearGradient(
                    colors: [Color(0xff703081), Color(0xff943FAB)], end: AlignmentDirectional.centerStart, begin: AlignmentDirectional.centerEnd)),
            padding: const EdgeInsets.symmetric(vertical: 10),
            child: const Text("قم بالحجز الآن", textAlign: TextAlign.center, style: TextStyle(fontSize: 22, color: Colors.white))));
  }
}