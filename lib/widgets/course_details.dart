import 'package:flutter/material.dart';

class CourseDetails extends StatelessWidget {
  const CourseDetails(
    this.value,
    this.icon, {
    Key? key,
  }) : super(key: key);
  final String value;
  final IconData icon;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 24),
      child: Row(
        children: <Widget>[
          Icon(icon, color: const Color(0xffb9bed1)),
          const SizedBox(width: 20),
          Expanded(child: Text(value)),
        ],
      ),
    );
  }
}