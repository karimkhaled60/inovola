import 'package:flutter/material.dart';
import 'package:innovola/provider/provider.dart';

class CustomSlider extends StatelessWidget {
  const CustomSlider(
    this.provider, {
    Key? key,
  }) : super(key: key);
  final ItemDetailsProvider provider;
  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: 200,
        width: MediaQuery.of(context).size.width,
        child: Stack(children: <Widget>[
          PageView.builder(
              itemCount: provider.place?.img?.length ?? 0,
              onPageChanged: provider.onPageChange,
              itemBuilder: (context, index) {
                return Image.network(provider.place?.img![index] ?? "",
                    errorBuilder: (_, __, ___) => Image.network(
                          "https://thumbs.dreamstime.com/z/four-friends-hands-up-creers-vecation-180552444.jpg",
                          fit: BoxFit.fill,
                        ),
                    width: MediaQuery.of(context).size.width,
                    fit: BoxFit.fill,
                    alignment: Alignment.topCenter);
              }),
          Positioned(
              top: 20 + MediaQuery.of(context).padding.top,
              width: MediaQuery.of(context).size.width,
              child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: Row(children: const <Widget>[
                    Icon(Icons.navigate_before, color: Colors.white),
                    Spacer(),
                    Icon(Icons.share, color: Colors.white),
                    SizedBox(width: 10),
                    Icon(Icons.favorite_border, color: Colors.white)
                  ]))),
          PositionedDirectional(
              bottom: 20,
              end: 24,
              child: Row(
                  children: List.generate(provider.place?.img?.length ?? 0, (index) {
                return Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 2),
                    child: CircleAvatar(
                        radius: provider.currentImage == index ? 7 : 5,
                        backgroundColor: provider.currentImage == index ? Colors.white : Colors.white.withOpacity(0.7)));
              })))
        ]));
  }
}
