import 'package:dio/dio.dart';

final dio = Dio(BaseOptions(baseUrl: "https://run.mocky.io/v3/"));

Future getData() async => (await dio.get("3a1ec9ff-6a95-43cf-8be7-f5daa2122a34")).data;
